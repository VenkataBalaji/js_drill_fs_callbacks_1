const { readLipsumFile,convertToUppercaseAndWite, processNewFile, sortAndWrite, deleteFiles }=require("../problem2.cjs");

readLipsumFile((data) => {
    convertToUppercaseAndWite(data, () => {
        processNewFile(() => {
            sortAndWrite(()=>{
                deleteFiles(()=>{        
            console.log("All processes completed successfully.");
                })
            })
        });
    });
});